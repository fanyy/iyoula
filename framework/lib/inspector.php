<?php

namespace framework
{
	protected $_class;

	protected $_meta = array(
		'class' => array(), 
		'properties' => array(), 
		'methods' => array()
	);

    protected $_properties = array();
    protected $_methods = array();

    public function __construct($class){
    	$this->_class = $class;
    }

    protected function _getClassComment(){
    	$reflection = new ReflectionClass($this->_class);
    	return $reflection->getDocComment();
    }

    protected function _getClassProperties(){
    	$refection = new ReflectionClass($this->_class);
    	return $reflection->getProperties();
    }

    protected function _getClassMethods()
    {
    	$refection = new ReflectionClass($this->_class);
    	return $reflection->getMethods();
    }

	protected function _getPropertyComment($property){
		$reflection = new ReflectionProperty($this->_class,$property);
		return $reflection->getDocComment();
	} 

	protected function _getMethodComment($method){
		$reflection = new ReflectionMethod($this->_class,$method);
		return $reflection->getDocComment();
	}
}